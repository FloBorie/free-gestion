# Free-Gestion

FreeGestion est un projet FullStack qui permet à des développeurs freelance de pouvoir manager les sites sur lesquels ils peuvent intervenir grâce à différents outils et différentes metrics.

```
cd existing_repo
git remote add origin https://gitlab.com/FloBorie/free-gestion.git
git branch -M main
git push -uf origin main
```



## Installation du projet en local 
Afin d'installer le projet en local, il faut récupérer toutes les dépendances npm. Pour faire cela, exécutez la commande suivante : 

````
npm install
````

Pour lancer l'application front, déplacez vous dans le dossier back et lancer le serveur : 

````
cd front/
nodemon index.js
````

Pour lancer l'application back, qui est constituée de l'api et d'une documentation (swagger), faites les commandes suivantes : 

`````
cd back/
nodemon index.js
`````

## Architecture de l'application
![alt text](./assets/architecture_application.png)

## Modélisation de la base de données
![alt text](./assets/uml_diagramme.png)

## Roadmap
![alt text](./assets/roadmap.png)