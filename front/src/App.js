// in src/App.js
import ContactSupport from "@material-ui/icons/ContactSupport";
import ErrorIcon from "@material-ui/icons/Error";
// Icon
import UserIcon from '@material-ui/icons/Group';
import SitesIcon from '@material-ui/icons/Language';
import jsonServerProvider from 'ra-data-json-server';
import * as React from "react";
import { Admin, Resource } from 'react-admin';
import { ContactClientsCreate, ContactClientsEdit, ContactClientsList } from "./component/contactClients";
import { ContactsCreate, ContactsEdit, ContactsList } from "./component/contacts";
import Dashboard from "./component/Dashboard";
import { ErrorsCreate, ErrorsEdit, ErrorsList } from "./component/errors";
import { SitesUrl, SitesUrlCreate, SitesUrlEdit } from "./component/sitesURL";
import authProvider from '../src/component/auth/authProvider';
import AnalyticsShow from "./component/Analytics/AnalyticsShow";




const dataProvider = jsonServerProvider('http://localhost:9000/api');

const App = () => (
  <Admin dataProvider={dataProvider} dashboard={Dashboard} authProvider={authProvider}>
    <Resource icon={UserIcon} name="contacts" list={ContactsList} edit={ContactsEdit} create={ContactsCreate} />
    <Resource icon={SitesIcon} name="sites" list={SitesUrl} edit={SitesUrlEdit} create={SitesUrlCreate} />
    <Resource icon={ErrorIcon} name="errors" list={ErrorsList} edit={ErrorsEdit} create={ErrorsCreate} />
    <Resource icon={ContactSupport} name="contactClients" list={ContactClientsList} edit={ContactClientsEdit} create={ContactClientsCreate} />
    <Resource name="Analytics" list={AnalyticsShow} icon={PostIcon} />
  </Admin>
);

export default App;