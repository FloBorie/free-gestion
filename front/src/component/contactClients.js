import * as React from "react";
import { Create, Datagrid, Edit, EditButton, List, NumberField, NumberInput, SimpleForm, TextField, TextInput } from 'react-admin';

export const ContactClientsList = props => (
    <List {...props}>
        <Datagrid rowClick="edit">
            
            <TextField source="firstName" />
            <TextField source="lastName" />
            <TextField source="email" />
            <TextField source="adresse" />
            <NumberField source="phoneNumber" />
            <TextField source="site"/>
            <TextField source="entreprise"/>
            <EditButton />
        </Datagrid>
    </List>
);

export const ContactClientsEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput source="firstName" />
            <TextInput source="lastName" />
            <TextInput source="email" />
            <TextInput source="adresse" />
            <NumberInput source="phoneNumber" />
            <TextInput source="site" />
            <TextInput source="entreprise" />
        </SimpleForm>
    </Edit>
);

export const ContactClientsCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="firstName" />
            <TextInput source="lastName" />
            <TextInput source="email" />
            <TextInput source="adresse" />
            <NumberInput source="phoneNumber" />
            <TextInput source="site" />
            <TextInput source="entreprise" />
        </SimpleForm>
    </Create>
);
