import * as React from "react";
import { Create, Datagrid, Edit, EditButton, List, NumberField, NumberInput, ReferenceField, ReferenceInput, SelectInput, SimpleForm, TextField, TextInput } from 'react-admin';

export const ContactsList = props => (
    <List {...props}>
        <Datagrid rowClick="edit">
            
            <TextField source="firstName" />
            <TextField source="lastName" />
            <TextField source="email" />
            <TextField source="adresse" />
            <NumberField source="phoneNumber" />
            <ReferenceField source="siteId" reference="sites">
                <TextField source="name" />
            </ReferenceField>
            <EditButton />
        </Datagrid>
    </List>
);

export const ContactsEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <ReferenceInput source="siteId" reference="sites">
                <SelectInput optionText="name" />
            </ReferenceInput>
            <TextInput source="firstName" />
            <TextInput source="lastName" />
            <TextInput source="email" />
            <TextInput source="adresse" />
            <NumberInput source="phoneNumber" />
        </SimpleForm>
    </Edit>
);

export const ContactsCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <ReferenceInput source="siteId" reference="sites">
                <SelectInput optionText="name" />
            </ReferenceInput>
            <TextInput source="firstName" />
            <TextInput source="lastName" />
            <TextInput source="email" />
            <TextInput source="adresse" />
            <NumberInput source="phoneNumber" />
        </SimpleForm>
    </Create>
);
