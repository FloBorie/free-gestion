import React, { useState, useEffect } from "react";
import Report from "../../report";
import { renderButton, checkSignedIn } from "../../utils";

function AnalyticsShow() {
    const [isSignedIn, setIsSignedIn] = useState(false);

    const updateSignin = (signedIn) => {
        setIsSignedIn(signedIn);
        if (!signedIn) {
            renderButton();
        }
    };

    const init = () => {
        checkSignedIn()
            .then((signedIn) => {
                updateSignin(signedIn);
            })
            .catch((error) => {
                console.error(error);
            });
    };

    useEffect(() => {
        window.gapi.load("auth2", init);
    });

    return (
        <div className="analytics-container">
            {!isSignedIn ? (
                <div id="signin-button"></div>
            ) : (
                <Report />
            )}
        </div>
    );
}

export default AnalyticsShow;