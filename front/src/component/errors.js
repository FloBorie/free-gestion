import * as React from "react";
import { Create, Datagrid, DateField, DateInput, Edit, EditButton, List, SimpleForm, TextField, TextInput } from 'react-admin';

export const ErrorsList = props => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <TextField source="description" />
            <DateField source="date" />
            <EditButton />
        </Datagrid>
    </List>
);

export const ErrorsEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput source="description" />
            <DateInput source="date" />
        </SimpleForm>
    </Edit>
);

export const ErrorsCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="description" />
            <DateInput source="date" />
        </SimpleForm>
    </Create>
);
