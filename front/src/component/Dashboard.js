import * as React from "react";
import { Card, CardContent, CardHeader } from '@material-ui/core';

export default () => (
    <Card>
        <CardHeader title="Bienvenue sur le panel pour gérer vos clients !" />
        <CardContent>Ceci permet d'avoir les informations de vos clients</CardContent>
    </Card>
);