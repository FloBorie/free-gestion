import * as React from "react";
import { List, Edit, SimpleForm, Datagrid, TextField, UrlField, BooleanField, NumberField, DateField, EditButton,TextInput, BooleanInput, NumberInput, DateInput, Create } from 'react-admin';

export const SitesUrl = props => (
    <List {...props}>
        <Datagrid rowClick="edit">
            
            <TextField source="name" />
            <UrlField source="url" />
            <TextField source="status" />
            <BooleanField source="sms" />
            <BooleanField source="email" />
            <NumberField source="timeTiking" />
            <DateField source="createdAt" />
            <DateField source="updatedAt" />
            <EditButton />
        </Datagrid>
    </List>
);

export const SitesUrlEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            
            <TextInput source="name" />
            <TextInput source="url" />
            <TextInput source="status" />
            <BooleanInput source="sms" />
            <BooleanInput source="email" />
            <NumberInput source="timeTiking" />
            <DateInput source="createdAt" />
            <DateInput source="updatedAt" />
        </SimpleForm>
    </Edit>
);

export const SitesUrlCreate = props => (
    <Create {...props}>
        <SimpleForm>
            
            <TextInput source="name" />
            <TextInput source="url" />
            <TextInput source="status" />
            <BooleanInput source="sms" />
            <BooleanInput source="email" />
            <NumberInput source="timeTiking" />
        </SimpleForm>
    </Create>
)

// Fetch pour récupérer le status d'un site en temps réel 

// fetch('https://cors-anywhere.herokuapp.com/' + url).then(function (response) {
//         console.log(response.status);
//         setStatus(response.status)
//       });