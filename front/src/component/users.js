import * as React from "react";
import { Datagrid, EditButton, List, ReferenceField, TextField } from 'react-admin';

export const UsersList = props => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <ReferenceField source="siteId" reference="sites">
                <TextField source="name" />
            </ReferenceField>
            <TextField source="firstName" />
            <TextField source="lastName" />
            <TextField source="email" />
            <TextField source="adresse" />
            <TextField source="role" />
            <EditButton />
        </Datagrid>
    </List>
);
