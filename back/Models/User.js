const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
    id: { type: String },
    firstName : { type: String, required: true },
    lastName : { type: String, required: true },
    email : { type: String, required: true, unique: true },
    adresse :{ type: String, required: true },
    password : { type: String, required: true },
    role : {
        type: String,
        enum: ['Admin'],
        default: 'Admin'
    },
    phoneNumber : { type: Number, required: true },
    siteId : { type: String, required: true }
}, { timestamps: true });

module.exports = mongoose.model("User", UserSchema)