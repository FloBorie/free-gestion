const mongoose = require("mongoose");

const ContactClientSchema = new mongoose.Schema({
    id: { type: String },
    firstName : { type: String, required: true },
    lastName : { type: String, required: true },
    email : { type: String, required: true },
    adresse : { type: String, required: true },
    phoneNumber : { type: Number, required : true },
    site : { type: String, required: true },
    entreprise : { type: String, required: true }
}, { timestamps: true })

module.exports = mongoose.model("ContactClient", ContactClientSchema)