const mongoose = require("mongoose");

const ContactSchema = new mongoose.Schema({
    id: { type: String },
    firstName : { type: String, required: true },
    lastName : { type: String, required: true },
    email : { type: String, required: true },
    adresse : { type: String, required: true },
    phoneNumber : { type: Number, required : true },
    siteId : { type: String },
    userId : { type: String }
}, { timestamps: true })

module.exports = mongoose.model("Contact", ContactSchema)