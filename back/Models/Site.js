const mongoose = require("mongoose");

const SiteSchema = new mongoose.Schema({
    id: { type: String },
    name: { type: String, required: true },
    url: { type: String, required: true },
    status: { type: String, required: true },
    sms: { type: Boolean, required: true },
    email: { type: Boolean, required: true },
    timeTiking: { type: Number, required: true },
    idUser: { type: String, required: true },
    idError: { type: String, required: true },
    idContact: { type: String, required: true }
}, { timestamps: true })

module.exports = mongoose.model("Site", SiteSchema)