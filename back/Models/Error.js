const mongoose = require("mongoose");

const ErrorSchema = new mongoose.Schema({
    id: { type: String },
    description : { type: String, required: true },
    date : { type: Date }
}, { timestamps: true })

module.exports = mongoose.model("Error", ErrorSchema)