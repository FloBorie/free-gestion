const express = require("express");
const app = express();
const dotenv = require("dotenv")
const userRoute = require("./routes/user")
const contactRoute = require("./Routes/contact")
const contactClientRoute = require("./Routes/contactclient")
const errorRoute = require("./Routes/error")
const siteRoute = require("./Routes/site")
const authRoute = require("./Routes/auth")
const cors = require("cors");
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');


dotenv.config()


const swaggerOptions = {
    swaggerDefinition:{
        openapi: "3.0.0",
        info: {
            title: 'Customer API',
            description: 'Customer API information',
            contact: {
            name: "amazing developper"
            },
            servers: [
                {
                    url: "http://localhost:9000",
                },
            ],
        }
    },
        apis: ["./Routes/*.js"]
  };
  
  const swaggerDocs = swaggerJsDoc(swaggerOptions);
  app.use("/api/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

const mongoose = require("mongoose")
mongoose.connect(process.env.MONGO_URL)
    .then(() => console.log("Database connected successfully"))
    .catch(err => console.error(err))

app.use(cors());
app.use(express.json());
app.use("/api/users", userRoute)
app.use("/api/contacts", contactRoute)
app.use("/api/contactClients", contactClientRoute)
app.use("/api/errors", errorRoute)
app.use("/api/sites", siteRoute)
app.use("/api/auth", authRoute)

app.listen(process.env.PORT || 9000, () => {
    console.log("Backend server is running!");
});