const router = require("express").Router();
const CryptoJS = require("crypto-js");
const User = require("../Models/User");

/**
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       required:
 *         - firstName
 *         - lastName
 *         - email
 *         - adresse
 *         - password
 *         - role
 *         - phoneNumber
 *         - siteNumber
 *       properties:
 *         firstName:
 *           type: string
 *           description:  Prénom du user
 *         lastName:
 *           type: string
 *           description: Nom du user
 *         email:
 *           type: string
 *           description: Mail du user
 *         adresse:
 *           type: string
 *           description:  Adresse du user
 *         password:
 *           type: string
 *           description: Mot de passe du user
 *         role:
 *           type: string
 *           description:  role du user
 *         phoneNumber:
 *           type: integer
 *           description:  numéro du user
 *         siteNumber:
 *           type: integer
 *           description:  Nom de site du user
 *       example:
 *         firstName: Guillaume ANTON
 *         lastName: ANTON
 *         email: guillaume.anton@ynov.com
 *         adresse: 37bis rue Poquelin Molière
 *         password: test
 *         role: Admin
 *         phoneNumber: 07892747280
 *         siteNumber: 10
 */

//UPDATE
/**
 * @swagger
 * /api/users/{id}:
 *  put:
 *    summary: Mise à jour du user par rapport à l'id
 *    tags: [User]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: L'id du user
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/User'
 *    responses:
 *      200:
 *        description: Le user à été mise jour
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/User'
 *      404:
 *        description: Le user n'est pas à jour
 *      500:
 *        description: Une erreur s'est produite
 */
router.put("/:id", async (req, res) => {
    if (req.body.password)
        req.body.password = CryptoJS.AES.encrypt(req.body.password, process.env.PASS_SEC).toString()

    try {
        const updatedUser = await User.findByIdAndUpdate(req.params.id, {
            $set: req.body
        }, { new: true })
        res.status(200).json(updatedUser)
    } catch (error) {
        res.status(500).json(error)
    }
})

//DELETE
/**
 * @swagger
 * /api/users/{id}:
 *   delete:
 *     summary: Supprimer un user selon l'id
 *     tags: [User]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: L'id du user
 * 
 *     responses:
 *       200:
 *         description: Le user est supprimé
 *       404:
 *         description: le user est introuvable
 */
router.delete("/:id", async (req, res) => {
    try {
        await User.findByIdAndDelete(req.params.id)
        res.status(200).json("User deleted successfully")
    } catch (error) {
        res.status(500).json(error)
    }
})

//GET USER
/**
 * @swagger
 * /api/users/{id}:
 *   get:
 *     summary: Permet d'accéder au user avec id
 *     tags: [User]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Le user selon l'id
 *     responses:
 *       200:
 *         description: Description du user
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       404:
 *         description: Le contact n'est pas défini
 */
router.get("/:id", async (req, res) => {
    try {
        const user = await User.findById(req.params.id)
        const {...otherParams } = user._doc
        res.status(200).json(otherParams)
    } catch (error) {
        res.status(500).json(error)
    }
})

//GET ALL USERS
/**
 * @swagger
 * /api/users:
 *  get:
 *    summary: Permet d'accéder à tous les users
 *    responses:
 *      '200':
 *        description: User bien trouvé
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/User'
 */
router.get("/", async (req, res) => {
    try {
        const users = await User.find()
        res.set('Content-Type', 'text/html')
        res.set('X-Total-Count', users.length)
        res.header('Access-Control-Expose-Headers', 'X-Total-Count')
        res.status(200).json(users)
    } catch (error) {
        res.status(500).json(error)
    }
})

module.exports = router