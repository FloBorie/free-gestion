const router = require("express").Router();
const CryptoJS = require("crypto-js");
const Error = require("../Models/Error");

/**
 * @swagger
 * components:
 *   schemas:
 *     Errors:
 *       type: object
 *       required:
 *         - description
 *       properties:
 *         description:
 *           type: string
 *           description:  description de l'erreur
 *       example:
 *         description: 500 Internal Server
 */

//CREATE

/**
 * @swagger
 * /api/errors:
 *   post:
 *     summary: Création d'une erreur
 *     tags: [Errors]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Errors'
 *     responses:
 *       200:
 *         description: L'erreur est bien créé
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Errors'
 *       500:
 *         description: Erreur de serveur
 */
router.post("/", async (req, res) => {
    const newError = new Error(req.body)
    newError.id = newError._id;
    try {
        const savedError = await newError.save();
        res.status(200).json(savedError)
    } catch (error) {
        res.status(500).json(error)
    }
})

//UPDATE
/**
 * @swagger
 * /api/errors/{id}:
 *  put:
 *    summary: Mise à jour d'une erreur par rapport à l'id
 *    tags: [Errors]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: L'id de l'erreur
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Errors'
 *    responses:
 *      200:
 *        description: L'erreur à été mise jour
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Errors'
 *      404:
 *        description: L'erreur n'a pas été mise à jour
 *      500:
 *        description: Une erreur s'est produite
 */
router.put("/:id", async (req, res) => {

    try {
        const updatedError = await Error.findByIdAndUpdate(req.params.id, {
            $set: req.body
        }, { new: true })
        res.status(200).json(updatedError)
    } catch (error) {
        res.status(500).json(error)
    }
})

//DELETE
/**
 * @swagger
 * /api/errors/{id}:
 *   delete:
 *     summary: Supprimer un erreur selon l'id
 *     tags: [Errors]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: L'id du erreur
 * 
 *     responses:
 *       200:
 *         description: Le erreur est supprimé
 *       404:
 *         description: le erreur est introuvable
 */
router.delete("/:id", async (req, res) => {
    try {
        await Error.findByIdAndDelete(req.params.id)
        res.status(200).json("Error deleted successfully")
    } catch (error) {
        res.status(500).json(error)
    }
})

//GET ERROR
/**
 * @swagger
 * /api/errors/{id}:
 *   get:
 *     summary: Permet d'accéder au erreur avec l'id
 *     tags: [Errors]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: L'erreur selon l'id
 *     responses:
 *       200:
 *         description: Description du Errors
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Errors'
 *       404:
 *         description: Le contact n'est pas défini
 */
router.get("/:id", async (req, res) => {
    try {
        const error = await Error.findById(req.params.id)
        const {...otherParams } = error._doc
        res.status(200).json(otherParams)
    } catch (error) {
        res.status(500).json(error)
    }
})

//GET ALL ERRORS
/**
 * @swagger
 * /api/errors:
 *  get:
 *    summary: Permet d'accéder à toutes les erreurs
 *    responses:
 *      '200':
 *        description: Erreur bien trouvé
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Errors'
 */
router.get("/", async (req, res) => {
    try {
        const errors = await Error.find()
        res.set('Content-Type', 'text/html')
        res.set('X-Total-Count', errors.length)
        res.header('Access-Control-Expose-Headers', 'X-Total-Count')
        res.status(200).json(errors)
    } catch (error) {
        res.status(500).json(error)
    }
})

module.exports = router