const router = require("express").Router();
const CryptoJS = require("crypto-js");
const ContactClient = require("../Models/ContactClient");

/**
 * @swagger
 * components:
 *   schemas:
 *     ContactClients:
 *       type: object
 *       required:
 *         - firstName
 *         - lastName
 *         - email
 *         - adresse
 *         - phoneNumber
 *         - site
 *         - entreprise
 *       properties:
 *         firstName:
 *           type: string
 *           description:  Prénom du contact
 *         lastName:
 *           type: string
 *           description: Nom du contact
 *         email:
 *           type: string
 *           description:  Mail du contact
 *         adresse:
 *           type: string
 *           description:  Adresse du contact
 *         phoneNumber:
 *           type: integer
 *           description: Numéro du contact
 *         site:
 *           type: string
 *           description:  Site du contact
 *         entreprise:
 *           type: string
 *           description:  Entreprise du contact
 *       example:
 *         firstName: Florian
 *         lastName: Borie
 *         email: florian.borie@ynov.com
 *         adresse: test
 *         phoneNumber: 07892747280
 *         site: florianlys.fr
 *         entreprise: Agirc-Arrco
 */

//CREATE
/**
 * @swagger
 * /api/contactClients:
 *   post:
 *     summary: Création d'un contact
 *     tags: [ContactClients]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/ContactClients'
 *     responses:
 *       200:
 *         description: Le contact est bien créé
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ContactClients'
 *       500:
 *         description: Erreur de serveur
 */
router.post("/", async (req, res) => {
    const newContactClient = new ContactClient(req.body)
    newContactClient.id = newContactClient._id;
    try {
        const savedContactClient = await newContactClient.save();
        res.status(200).json(savedContactClient)
    } catch (error) {
        res.status(500).json(error)
    }
})

//UPDATE
/**
 * @swagger
 * /api/contactClients/{id}:
 *  put:
 *    summary: Mise à jour du contact par rapport à l'id
 *    tags: [ContactClients]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: L'id du contact
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/ContactClients'
 *    responses:
 *      200:
 *        description: Le contact à été mise jour
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ContactClients'
 *      404:
 *        description: Le contact n'est pas à jour
 *      500:
 *        description: Une erreur s'est produite
 */
router.put("/:id", async (req, res) => {

    try {
        const updatedContactClient = await ContactClient.findByIdAndUpdate(req.params.id, {
            $set: req.body
        }, { new: true })
        res.status(200).json(updatedContactClient)
    } catch (error) {
        res.status(500).json(error)
    }
})

//DELETE
/**
 * @swagger
 * /api/contactClients/{id}:
 *   delete:
 *     summary: Supprimer un contact selon l'id
 *     tags: [ContactClients]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: L'id du contact
 * 
 *     responses:
 *       200:
 *         description: Le contact est supprimé
 *       404:
 *         description: le contacts est introuvable
 */
router.delete("/:id", async (req, res) => {
    try {
        await ContactClient.findByIdAndDelete(req.params.id)
        res.status(200).json("ContactClient deleted successfully")
    } catch (error) {
        res.status(500).json(error)
    }
})

//GET USER
/**
 * @swagger
 * /api/contactClients/{id}:
 *   get:
 *     summary: Permet d'accéder au contact avec id
 *     tags: [ContactClients]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Le contact selon l'id
 *     responses:
 *       200:
 *         description: Description du contacts
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ContactClients'
 *       404:
 *         description: Le contact n'est pas défini
 */
router.get("/:id", async (req, res) => {
    try {
        const contactClient = await ContactClient.findById(req.params.id)
        const {...otherParams } = contactClient._doc
        res.status(200).json(otherParams)
    } catch (error) {
        res.status(500).json(error)
    }
})

//GET ALL USERS
/**
 * @swagger
 * /api/contactClients:
 *  get:
 *    summary: Permet d'accéder à tous les contacts
 *    responses:
 *      '200':
 *        description: Contact bien trouvé
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/ContactClients'
 */
router.get("/", async (req, res) => {
    try {
        const contactClients = await ContactClient.find()
        res.set('Content-Type', 'text/html')
        res.set('X-Total-Count', contactClients.length)
        res.header('Access-Control-Expose-Headers', 'X-Total-Count')
        res.status(200).json(contactClients)
    } catch (error) {
        res.status(500).json(error)
    }
})

module.exports = router