const router = require("express").Router();
const CryptoJS = require("crypto-js");
const Contact = require("../Models/Contact");

/**
 * @swagger
 * components:
 *   schemas:
 *     Contacts:
 *       type: object
 *       required:
 *         - firstName
 *         - lastName
 *         - email
 *         - adresse
 *         - phoneNumber
 *         - site
 *         - idUser
 *       properties:
 *         firstName:
 *           type: string
 *           description:  Prénom du contact
 *         lastName:
 *           type: string
 *           description: Nom du contact
 *         email:
 *           type: string
 *           description:  Mail du contact
 *         adresse:
 *           type: string
 *           description:  Adresse du contact
 *         phoneNumber:
 *           type: integer
 *           description: Numéro du contact
 *         site:
 *           type: string
 *           description:  Site du contact
 *         idUser:
 *           type: integer
 *           description:  Id du contact
 *       example:
 *         firstName: Florian
 *         lastName: Borie
 *         email: florian.borie@ynov.com
 *         adresse: test
 *         phoneNumber: 07892747280
 *         site: florianlys.fr
 *         idUser: 623c3f8fc3300a8d311dddc5
 */

//CREATE
/**
 * @swagger
 * /api/contacts:
 *   post:
 *     summary: Création d'un contact
 *     tags: [Contacts]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Contacts'
 *     responses:
 *       200:
 *         description: Le contact est bien créé
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Contacts'
 *       500:
 *         description: Erreur de serveur
 */
router.post("/", async (req, res) => {
    const newContact = new Contact(req.body)
    newContact.id = newContact._id;
    try {
        const savedContact = await newContact.save();
        res.status(200).json(savedContact)
    } catch (error) {
        res.status(500).json(error)
    }
})

//UPDATE
/**
 * @swagger
 * /api/contacts/{id}:
 *  put:
 *    summary: Mise à jour du contact par rapport à l'id
 *    tags: [Contacts]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: L'id du contact
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Contacts'
 *    responses:
 *      200:
 *        description: Le contact à été mise jour
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Contacts'
 *      404:
 *        description: Le contact n'est pas à jour
 *      500:
 *        description: Une erreur s'est produite
 */
router.put("/:id", async (req, res) => {

    try {
        const updateContact = await Contact.findByIdAndUpdate(req.params.id, {
            $set: req.body
        }, { new: true })
        res.status(200).json(updateContact)
    } catch (error) {
        res.status(500).json(error)
    }
})

//DELETE
/**
 * @swagger
 * /api/contacts/{id}:
 *   delete:
 *     summary: Supprimer un contact selon l'id
 *     tags: [Contacts]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: L'id du contact
 * 
 *     responses:
 *       200:
 *         description: Le contact est supprimé
 *       404:
 *         description: le contacts est introuvable
 */
router.delete("/:id", async (req, res) => {
    try {
        await Contact.findByIdAndDelete(req.params.id)
        res.status(200).json("Contact deleted successfully")
    } catch (error) {
        res.status(500).json(error)
    }
})

//GET USER
/**
 * @swagger
 * /api/contacts/{id}:
 *   get:
 *     summary: Permet d'accéder au contact avec id
 *     tags: [Contacts]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Le contact selon l'id
 *     responses:
 *       200:
 *         description: Description du contacts
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Contacts'
 *       404:
 *         description: Le contact n'est pas défini
 */
router.get("/:id", async (req, res) => {
    try {
        const contact = await Contact.findById(req.params.id)
        const { ...otherParams } = contact._doc
        res.status(200).json(otherParams)
    } catch (error) {
        res.status(500).json(error)
    }
})

//GET ALL USERS
/**
 * @swagger
 * /api/contacts:
 *  get:
 *    summary: Permet d'accéder à tous les contacts
 *    responses:
 *      '200':
 *        description: Contact bien trouvé
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Contacts'
 */
router.get("/", async (req, res) => {
    try {
        const contacts = await Contact.find()
        res.set('Content-Type', 'text/html')
        res.set('X-Total-Count', contacts.length)
        res.header('Access-Control-Expose-Headers', 'X-Total-Count')
        res.status(200).json(contacts)
    } catch (error) {
        res.status(500).json(error)
    }
})

module.exports = router