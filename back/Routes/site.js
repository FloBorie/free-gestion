const router = require("express").Router();
const CryptoJS = require("crypto-js");
const Site = require("../Models/Site");

/**
 * @swagger
 * components:
 *   schemas:
 *     Sites:
 *       type: object
 *       required:
 *         - name
 *         - url
 *         - status
 *         - sms
 *         - email
 *         - timeTiking
 *         - idUser
 *         - idError
 *         - idContact
 *       properties:
 *         name:
 *           type: string
 *           description:  nom du site
 *         url:
 *           type: string
 *           description: URL du site
 *         status:
 *           type: string
 *           description: Statut du site
 *         sms:
 *           type: boolean
 *           description:  sms du site
 *         email:
 *           type: boolean
 *           description: email du site
 *         timeTiking:
 *           type: integer
 *           description:  timeTiking du site
 *         idUser:
 *           type: string
 *           description:  Id user du site
 *         idError:
 *           type: string
 *           description:  Id d'erreur du site
 *         idContact:
 *           type: string
 *           description:  Id contact du site
 *       example:
 *         name: Guillaume ANTON
 *         url: guillaumeanton.fr
 *         status: Online
 *         sms: false
 *         email: true
 *         timeTiking: 10
 *         idUser: 623c3f8fc3300a8d311dddc5
 *         idError: 623c4182e41fc439b75b3537
 *         idContact: 623c4089735cc58bda6b1394
 */

//CREATE
/**
 * @swagger
 * /api/sites:
 *   post:
 *     summary: Création d'un site
 *     tags: [Sites]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Sites'
 *     responses:
 *       200:
 *         description: Le site est bien créé
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Sites'
 *       500:
 *         description: Erreur de serveur
 */
router.post("/", async (req, res) => {
    const newSite = new Site(req.body)
    newSite.id = newSite._id;
    try {
        const savedSite = await newSite.save();
        res.status(200).json(savedSite);
    } catch (error) {
        res.status(500).json(error)
    }
})

//UPDATE 
/**
 * @swagger
 * /api/sites/{id}:
 *  put:
 *    summary: Mise à jour du site par rapport à l'id
 *    tags: [Sites]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: L'id du site
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Sites'
 *    responses:
 *      200:
 *        description: Le site à été mise jour
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Sites'
 *      404:
 *        description: Le site n'est pas à jour
 *      500:
 *        description: Une erreur s'est produite
 */
router.put("/:id", async(req, res) => {
    try {
        const updateSite = await Site.findByIdAndUpdate(req.params.id, {
            $set: req.body
        }, { new: true })
        res.status(200).json(updateSite)

    } catch (error) {
        res.status(500).json(error)
    }
});

//DELETE
/**
 * @swagger
 * /api/sites/{id}:
 *   delete:
 *     summary: Supprimer un site selon l'id
 *     tags: [Sites]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: L'id du site
 * 
 *     responses:
 *       200:
 *         description: Le site est supprimé
 *       404:
 *         description: le site est introuvable
 */
router.delete("/:id", async (req, res) => {
    try {
        await Site.findByIdAndDelete(req.params.id)
        res.status(200).json("Site deleted successfully")
    } catch (error) {
        res.status(500).json(error)
    }
})

//GET SITE
/**
 * @swagger
 * /api/sites/{id}:
 *   get:
 *     summary: Permet d'accéder au site avec id
 *     tags: [Sites]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Le site selon l'id
 *     responses:
 *       200:
 *         description: Description du site
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Sites'
 *       404:
 *         description: Le contact n'est pas défini
 */
router.get("/:id", async (req, res) => {
    try {
        const site = await Site.findById(req.params.id)
        const {...otherParams } = site._doc
        res.status(200).json(otherParams)
    } catch (error) {
        res.status(500).json(error)
    }
})

//GET ALL SITES
/**
 * @swagger
 * /api/sites:
 *  get:
 *    summary: Permet d'accéder à tous les sites
 *    responses:
 *      '200':
 *        description: Site bien trouvé
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Sites'
 */
router.get("/", async (req, res) => {
    try {
        const sites = await Site.find()
        
        res.set('Content-Type', 'text/html')
        res.set('X-Total-Count', sites.length)
        res.header('Access-Control-Expose-Headers', 'X-Total-Count')
        res.status(200).json(sites);

    } catch (error) {
        res.status(500).json(error)
    }
})

module.exports = router